const n = parseInt(prompt("Choose a number"))
console.log (`You have chosen ${n}`)

const getCube = n ** 3
console.log(`The cube of ${n} is ${getCube}`)

const Address = ["420" , "Mirepoix" , "Houston", "Colorado" , "90210"]

const [lotNum, Street, City, State, ZipCode] = Address

console.log(`I live at ${lotNum} ${Street} Street, ${City}, ${State} ${ZipCode}.`)

let animal = {
	animal: "Cat",
	name: "Pillows",
	breed: "Siamese Half-breed",
	weight: "fat",
	hobbies: "eat and sleep"
}

console.log (`${animal.name} is a ${animal.breed} ${animal.animal}. He is ${animal.weight} and really loves to ${animal.hobbies}.`)

const array = [34, 23, 10, 90, 14, 2]

array.forEach((array) => {
	console.log (`${array}`)
})

class Dog {
	constructor(name,age,breed){
		this.name = name,
		this.age = age,
		this.breed = breed
	}
}

const myNewDog = new Dog ("Ein",3,"Welsh Corgi")
console.log(myNewDog)